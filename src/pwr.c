/*
 * Copyright (C) 2019 Aidan Williams
 *
 * pwr is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pwr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pwr.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <errno.h>
#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Platform specific includes
#if defined (__linux__)
#include <glob.h>
#endif

#if defined (__FreeBSD__) || defined (__DragonFly__)
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <dev/acpica/acpiio.h>
#endif

#include "xalloc.h"
#include "xprintf.h"
#include "xvasprintf.h"

// Default output format
#define PWRFORMAT "\\s\n"

// String printed out with --version
static const char *versionstr =
    PACKAGE_STRING "\n"
    "Copyright (C) 2019 Aidan Williams\n"
    "License GPLv3+: GNU GPL version 3 or later "
    "<https://gnu.org/licenses/gpl.html>\n"
    "This is free software: you are free to change and redistribute it.\n"
    "There is NO WARRANTY, to the extent permitted by law.\n\n"
    "Written by Aidan Williams\n";

// String printed out with --help
static const char *usagestr =
    "Usage: " PACKAGE " [OPTIONS]\n"
    "Options:\n"
    "  -f, --format  \tFormats output by argument given\n"
    "  -s, --single  \tGets power from battery defined by argument given\n"
    "      --help    \tPrints this help text\n"
    "      --version \tPrints the version information\n";

static struct option lopts[] = {
    { "format",  required_argument, 0, 'f' },
    { "single",  required_argument, 0, 's' },
    { "help",    no_argument,       0,  0  },
    { "version", no_argument,       0,  0  },
    { 0,         0,                 0,  0  }
};

/*
 * Define escape char
 * On GNU it can be expressed as '\e', which increases portibility
 */
#ifdef __GNUC__
#define ESCP '\e'
#else
#define ESCP 033
#endif

/*
 * Prints formatted output safely with a subset of escape chars
 *
 * Additionally, the escape '\s' allows the int passed as 'arg' to appear
 * in the output
 */
void printe(char *str, int arg)
{
    int c;

    while ((c = *str++) != '\0') {
        if (c != '\\') {
            xprintf("%c", c);
            continue;
        }

        c = *str++;

        switch (c) {
        case 'a':
            xprintf("\a");
            break;
        case 'b':
            xprintf("\b");
            break;
        case 'e':
            xprintf("%c", ESCP);
            break;
        case 'f':
            xprintf("\f");
            break;
        case 'n':
            xprintf("\n");
            break;
        case 'r':
            xprintf("\r");
            break;
        case 't':
            xprintf("\t");
            break;
        case 'v':
            xprintf("\v");
            break;
        case 's':
            xprintf("%i", arg);
            break;
        default:
            xprintf("\\%c", c);
            break;
        }
    }
}

// Prints usage and exits
void usage(int err)
{
    // Note that err is expected to be 0 when succsessful
    xfprintf(err ? stderr : stdout, "%s", usagestr);
    exit(err);
}

// Prints version and exits
void version()
{
    xprintf("%s", versionstr);
    exit(0);
}

#if defined (__linux__)
/*
 * Internal for getting power from the path of a sysfs battery
 * Specific to GNU/Linux with sysfs
 */
int sysfspwr(const char *path)
{
    FILE *fptr;
    int percent = 0;

    if ((fptr = fopen(path, "r"))) {
        fscanf(fptr, "%i", &percent);
        fclose(fptr);
    } else {
        xfprintf(stderr, PACKAGE": %s: %s\n", strerror(errno), path);
        exit(errno);
    }

    return percent;
}
#endif

#if defined (__FreeBSD__) || defined (__DragonFly__)
/*
 * Internal for getting power from the id of a battery via the acpi api
 * Specific to (Free/Dragonfly)BSD, API is not documented outside of header file
 * OS API may be unstable, especially on DragonFlyBSD
 */
int bsdpwr(int bat)
{
    static int acpi;

    // Try opening ACPI kernel interface
    acpi = open("/dev/acpi", O_RDWR);

    if (acpi < 0)
        acpi = open("/dev/acpi", O_RDONLY);

    if (acpi < 0) {
        xfprintf(stderr, PACKAGE": %s: %s\n", strerror(errno), "/dev/acpi");
        exit(errno);
    }

    // Ask kernel nicely for battery information
    union acpi_battery_ioctl_arg battio;

    battio.unit = bat;
    if (ioctl(acpi, ACPIIO_BATT_GET_BATTINFO, &battio) < 0) {
        xfprintf(stderr, PACKAGE": %s: /dev/acpi (unit %i)\n",
                 strerror(errno), bat);
        exit(EXIT_FAILURE);
    }

    int percent = battio.battinfo.cap;

    close(acpi);
    return percent;
}
#endif

// Gets the average power of all of the system batteries
int pwr()
{
#if defined (__linux__)
    glob_t glb;

    // GLOB_NOSORT is for speed
    glob("/sys/class/power_supply/*/capacity", GLOB_NOSORT, 0, &glb);

    // glb.gl_pathc is truthy even when it's a nonzero number
    if (glb.gl_pathc < 1) {
        globfree(&glb);
        xfprintf(stderr, PACKAGE": No batteries found\n");
        exit(ENODEV);
    }

    int avrg = 0;
    int avgtot = 0;

    // Averages battery
    for (size_t i = 0; i < glb.gl_pathc; i++)
        avgtot += sysfspwr(glb.gl_pathv[i]);

    avrg = avgtot / glb.gl_pathc;

    globfree(&glb);
    return avrg;
#elif defined (__FreeBSD__) || (__DragonFly__)
    return bsdpwr(ACPI_BATTERY_ALL_UNITS);
#else
    xfprintf(stderr, PACKAGE": Unsupported platform\n");
    exit(EXIT_FAILURE);
#endif
}


// Gets the power of the specified battery
int fpwr(const char *bat)
{
#if defined (__linux__)
    char *tmp = xasprintf("/sys/class/power_supply/BAT%s/capacity", bat);

    int batpwr = sysfspwr(tmp);
    free(tmp);

    return batpwr;
#elif defined (__FreeBSD__) || defined (__DragonFly__)
    return bsdpwr(atoi(bat));
#else
    xfprintf(stderr, "Unsupported platform\n");
    exit(EXIT_FAILURE);
#endif
}

int main(int argc, char **argv)
{
    int opt, opti;
    char *arg;
    char *battery = NULL;
    char *pwrfmt = PWRFORMAT;

   while ((opt = getopt_long(argc, argv, "mf:s:hv", lopts, &opti)) != -1) {
        switch (opt) {
        case 0:
            arg = (char *) lopts[opti].name;
            if (strcmp("help", arg) == 0)
                usage(0);
            else
                version();
        case 'f':
            pwrfmt = optarg;
            break;
        case 's':
            battery = optarg;
            break;
        default:
            usage(EINVAL);
        }
    }

    printe(pwrfmt, battery ? fpwr(battery) : pwr());
    return 0;
}
