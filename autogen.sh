#!/bin/sh
# shell script for initializing autoconf
#
# Copyright (C) 2019 Aidan Williams
#
# pwr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pwr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pwr.  If not, see <http://www.gnu.org/licenses/>.

git submodule update --init --recursive

# List of gnulib modules needed by program
MODULES="xvasprintf xprintf xalloc getopt-gnu"

./gnulib/gnulib-tool --no-vc-files --import $MODULES
autoreconf -fiv
